## Garuda Linux XFCE

## Garuda Specific
garuda-settings-manager-notifier

## Shell
bash
bash-completion

## Audio
paprefs
pasystray
pavucontrol
playerctl
pulseaudio-ctl
pulseaudio-equalizer-ladspa
pulseaudio-support
qjackctl

## Video
gstreamer-meta
redshift

## Network
blueman
networkmanager-support
network-manager-applet
modem-manager-gui
qomui

## Input
gestures

## Utilities
## Desktop Utilities
autorandr
xdg-desktop-portal
xdg-desktop-portal-gtk

## XORG
xorg-server
xorg-server-xephyr
xorg-xwininfo
xorg-xhost
xorg-xinit
xorg-xinput
xorg-xrandr
xorg-xprop
xorg-xkill
xorg-xbacklight
xorg-xsetroot

## XORG Applications
numlockx
wmctrl
xbindkeys
xcape
xdotool
xautolock

## Theme Packages
garuda-xfce-settings
gtk-engine-murrine
kvantum-qt5
papirus-icon-theme
plymouth-theme-garuda
qt5ct

## Display Manager
lightdm
lightdm-slick-greeter
lightdm-settings
accountsservice  # Enhanced user accounts handling

## Desktop Environment - XFCE
exo
garcon
thunar
thunar-volman
tumbler
xfce4-appfinder
xfce4-panel
xfce4-power-manager
xfce4-session
xfce4-settings
xfce4-terminal
xfconf
xfdesktop
xfwm4
xfwm4-themes

## File Manager Extensions
ffmpegthumbnailer
libgsf
libopenraw
librsvg
gvfs-mtp
gvfs
gvfs-afc
gvfs-nfs
gvfs-smb
gvfs-gphoto2
gvfs-google
gvfs-goa

poppler-data
poppler-glib
xarchiver

## XFCE Extra
menulibre
mugshot
mousepad
ristretto
thunar-archive-plugin
thunar-media-tags-plugin
xfce4-battery-plugin
xfce4-clipman-plugin
xfce4-datetime-plugin
xfce4-dict
xfce4-notes-plugin
xfce4-notifyd
xfce4-pulseaudio-plugin
xfce4-screensaver
xfce4-screenshooter
xfce4-weather-plugin
xfce4-xkb-plugin
xfce4-mount-plugin
xfce4-whiskermenu-plugin
xfce4-taskmanager

## Not needed but nice to have
xfce4-cpufreq-plugin
xfce4-cpugraph-plugin
xfce4-diskperf-plugin
xfce4-eyes-plugin
xfce4-fsguard-plugin
xfce4-genmon-plugin
xfce4-mailwatch-plugin
xfce4-mpc-plugin
xfce4-netload-plugin
xfce4-sensors-plugin 
xfce4-smartbookmark-plugin
xfce4-systemload-plugin
xfce4-time-out-plugin
xfce4-timer-plugin
xfce4-verve-plugin
xfce4-wavelan-plugin

## Accessibility
orca
festival
onboard
mousetweaks

## Multimedia
celluloid # GTK+ front-end for mpv
pitivi
audacity

## Graphics
gcolor3
pinta

## Office
abiword
libmythes

## Internet Applications
firefox
firefox-ublock-origin
geary
transmission-gtk

## Security
gtkhash
veracrypt

## AMD Vulkan
amdvlk
lib32-amdvlk

## Miscellaneous Application
gnome-keyring
qpdfview
galculator
gparted
catfish
meld
nano
neofetch
peek
mintstick
bleachbit
xfburn
systemd-ui
python-xdg
vim # Needed for pacdiff
xboxdrv
